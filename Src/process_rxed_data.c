

#include "main.h"
#include "stm32f0xx_hal.h"


#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include "pt.h"
#include "common_types.h"
#include "utils.h"
#include "data.h"

#define MS_IN_SEC  (1000UL)
#define MS_IN_MIN  (60*MS_IN_SEC)
#define MS_IN_HOUR (60*MS_IN_MIN)

#define RX_SIZE 1030

u16 crc();



u8 buffer_rx[RX_SIZE];
u8* buffer_rx_data = &buffer_rx[4];
u8 cnt_rx;
u8 uart_rx = 0;

u8 buffer_tx[RX_SIZE];
u8* buffer_tx_data = &buffer_tx[4];
u8 tx_data_size;

u8 own_number[16] = "123";
u8 firmware_version[16] = "456";


/*
�������	��������� �������
2h	07h
3h	04h 11h 15h 17h
4h	05h
5h	no
6h
7h	03h 18h 12h 14h
10h	11h 17h 15h 04h
11h	07h
12h	13h 18h 10h
13h	07h
14h	11h 17h 15h 04h
15h	07h
16h	11h 17h 15h 04h
17h	07h
18h	19h
19h	13h 18h 10h
*/

s8 is_possible_state(u8 prev_cmd, u8 cur_cmd) {

    switch(prev_cmd)
    {
        // wait for data
        case 0x02:
        case 0x11:
        case 0x13:
        case 0x15:
        case 0x17:
        case 0x18:          
            if (0x07 == cur_cmd) {
                return 0;
            }
        break;
        // FISCAL
        case 0x03:
        case 0x10:
        case 0x14:
        // case 0x19:
            if ((0x04 == cur_cmd)|
                (0x10 == cur_cmd)|
                (0x11 == cur_cmd)|
                (0x18 == cur_cmd)) {
                return 0;
            }
        break;
        case 0x04:
            if (0x05 == cur_cmd) {
                return 0;
            }
        break;
        case 0x05:
        case 0x06:
            while(1)
        break;
        // data rxed
        case 0x07:
            if ((0x03 == cur_cmd)|
                (0x07 == cur_cmd)|
                (0x12 == cur_cmd)|
                (0x14 == cur_cmd)|
                (0x16 == cur_cmd)|
                (0x19 == cur_cmd)){
                 return 0;
            }
        break;
        // smenaopen
        case 0x12:
            if ((0x10 == cur_cmd)|
                (0x15 == cur_cmd)|
                (0x17 == cur_cmd)|
                (0x13 == cur_cmd)){
                 return 0;
            }
        break;
        case 0x19:
            // FISCAL
            if ((0x04 == cur_cmd)|
                (0x10 == cur_cmd)|
                (0x11 == cur_cmd)|
                (0x18 == cur_cmd)){
                return 0;
            }
            // POSTFISCAL
            if (0x18 == cur_cmd) {
                return 0;
            }
        break;

        default:
        break;
    }


    return -1;
}











void process_rxed_data() {

    u16 len;
    static u8 prev_cmd = 0;

    if (0){//(!checkcrc(buffer_rx, cnt_rx))     {
        answer_code = ERR_CODE_01;
        tx_data_size = 0;
        return;
    }

    if (MSG_START != buffer_rx[0]) {
        answer_code = ERR_CODE_01;
        tx_data_size = 0;
        return;
    }

    len = ((u16)buffer_rx[1]<<8) + (u16)buffer_rx[2] + 1;
    if (len > 1030) {
        answer_code = ERR_CODE_01;
        tx_data_size = 0;
        return;
    }
    if (len != (cnt_rx - 7)) {
        answer_code = ERR_CODE_01;
        tx_data_size = 0;
        return;
    }

    if ((0x02 != buffer_rx[3]) && (READY2FISCAL != State)) {
        if (0 != is_possible_state(prev_cmd, buffer_rx[3])) {

            answer_code = ERR_CODE_03;
            tx_data_size = 0;
            return;
        }
    }

    answer_code = ERR_CODE_00;
    tx_data_size = 0;

    switch(buffer_rx[3])
    {
        // ** 6.1
        // ** 6.2 ��������� ������� ��
        // --------------------------------------------------------------------
        // --------------------------------------------------------------------
        case 0x30: // ������ ������� �� 19
            prev_cmd = buffer_rx[3];
            buffer_tx_data[0] = life_phase;
            buffer_tx_data[1] = cur_doc;
            buffer_tx_data[2] = doc_data;
            buffer_tx_data[3] = state_of_work;
            buffer_tx_data[4] = flags_and_asserts;
            memcpy(&buffer_tx_data[5], date_time_last_doc, 5);
            memcpy(&buffer_tx_data[10], own_number, sizeof(own_number));
            memcpy(&buffer_tx_data[26], &last_fd_number, sizeof(last_fd_number));
            answer_code = ERR_CODE_00;
            tx_data_size = 34;
          return;
        break;
        case 0x31: // ������ ������ ��
            prev_cmd = buffer_rx[3];
            memcpy(buffer_tx_data, own_number, sizeof(own_number));
            answer_code = ERR_CODE_00;
            tx_data_size = sizeof(own_number);
          return;
        break;
        case 0x32: // ������ ����� �������� ��
            prev_cmd = buffer_rx[3];
            memcpy(buffer_tx_data, date_expired, 3);
            buffer_tx_data[4] = 0;
            buffer_tx_data[6] = 0;
            tx_data_size = 6;
          return;
        break;
        case 0x33: // ������ ������ ��
            prev_cmd = buffer_rx[3];
            memcpy(buffer_tx_data, firmware_version, sizeof(firmware_version));
            buffer_tx_data[17] = debug_mode;
            answer_code = ERR_CODE_00;
            tx_data_size = 17;
          return;
        break;
        case 0x35: // ������ ��������� ������ ��
            prev_cmd = buffer_rx[3];
            memset(buffer_tx_data, 'A', 5);
            answer_code = ERR_CODE_00;
            tx_data_size = 5;
          return;
        break;

        // ** 6.3 ����� ������� ��� ������������ ���������� ����������
        // --------------------------------------------------------------------
        // --------------------------------------------------------------------
        case 0x06: // �������� ��������
            prev_cmd = buffer_rx[3];
            answer_code = ERR_CODE_00;
            tx_data_size = 0;
          return;
        break;
        case 0x07: // �������� ������ ���������
        /*
        �������  �������������  ���  ��������  ��  ���  �  ��  ������  ��������
        �����������  ���������.  ������  ����������  ���  ������  TLV  ��������.  ��
        ������������ ������������ TLV (������������ ���� ����� ����������� �����),
        �������  ����������  ����������  ������  �����  TLV  �������  (��  ����  ������
        ��������� ���� ������ �� 2 �������).
        ��������� ����� ���� ������, ����������� � ������� ������ �������,
        �������  ��  ����  �����������  ���������  �  ����������  �  ��������  �������
        ������� ...� ��� ������� �� ���������� ����������.

        �� �� ��������� ��������0���������� �������� TLV ������.
        ��������� �������� ������ ������� ��������� ���, ��� �������� ����
        ����������� ������.
        */
            // ���������� �������� TLV ������
            if (errno == ERR_CODE_10) {
                answer_code = ERR_CODE_10;
                tx_data_size = 0;
                return;
            }
            // ������ ��
            if (errno == ERR_CODE_03) {
                answer_code = ERR_CODE_03;
                tx_data_size = 0;
                return;
            }
            
            
            if (REG1 == State) {
                // ������������ ����� ������ 2�
                State = REG2;
            } else if (OP1 == State) {
                // ������������ ����� ������ 1�
                State = OP2;
            } else if (CLOS1 == State) {
                // ������������ ����� ������ 1�
                State = CLOS2;
            } else if (CHK1 == State) {
                // ������������ ����� ������ 30�
                State = CHK2;
            } else if (REPORT1 == State) {
                // ������������ ����� ������ 1�
                State = REPORT2;
            } else {
                answer_code = ERR_CODE_02;
                tx_data_size = 0;
                return;
            }
            prev_cmd = buffer_rx[3];
            answer_code = ERR_CODE_00;
            tx_data_size = 0;
          return;
        break;


        // ** 6.4 ������� ������������ � ���������� ����������� ������
        // --------------------------------------------------------------------
        // --------------------------------------------------------------------
        case 0x02: // ������ ����� � ����������� ���  (������������ ��)
        /*
        ������� �������� ������������ ������ �� ��������� �������:
          ����� � ����������� ���
          ����� �� ��������� ���������� ����������� ���, � ����� � ������� ��
          ����� �� ��������� ���������� ����������� ��� ��� ������ ��

          �����  ����������  ����  �������  ��  �������  ���������  ��������������
        ������ � ������� ������� ��������� ������ ���������. ������������
        ������ ������������ ������ �� ����� ��������� 2 ���������.
        */
            if (READY2FISCAL != State) {
                answer_code = ERR_CODE_02;
                tx_data_size = 0;
                return;
            }
            prev_cmd = buffer_rx[3];
            State = REG1;

            report_type = buffer_rx_data[0];
            answer_code = ERR_CODE_00;
            tx_data_size = 0;

            return;
        break;
        case 0x03: // ������������ ����� � ����������� (���������������) ���
        /*
        ������ ������� ��������� ������������ ������ � ����������� ��� �
        ��������� �� � ���������� �����. �� � ������ ������ ���� ���������
        ������� ������� ����� � ����������� (���������������) ��� 02h� � ���������
        ������ ��������� 07h�
        */
            //sprintf((char*)buffer_tx_data, "%s", date_time_cur_doc);

            // ������ ��
            if (errno == ERR_CODE_03) {
                answer_code = ERR_CODE_03;
                tx_data_size = 0;
                return;
            }
            // ������ ��
            if (errno == ERR_CODE_04) {
                answer_code = ERR_CODE_04;
                tx_data_size = 0;
                return;
            }
            // ������������ �������� ���������� �������
            if (errno == ERR_CODE_09) {
                answer_code = ERR_CODE_09;
                tx_data_size = 0;
                return;
            }
            // �������� ���� �/��� �����
            if (errno == ERR_CODE_07) {
                answer_code = ERR_CODE_07;
                tx_data_size = 0;
                return;
            }
            // �������� ��������� ��
            if (REG2 != State) {
                answer_code = ERR_CODE_02;
                tx_data_size = 0;
                return;
            }
            prev_cmd = buffer_rx[3];
            State = FISCAL;
            life_phase = LIFE_PHASE_FISCAL;
            
            memcpy(date_time_cur, &buffer_rx_data[0], 5);
            memcpy(inn_number, &buffer_rx_data[0+5], 12);
            memcpy(kkt_number, &buffer_rx_data[0+5+12], 20);
            tax_code = buffer_rx_data[0+5+12+20];
            mode_of_work = buffer_rx_data[0+5+12+20+1];

            if (REPORT_TYPE_02 == report_type ) {
                tx_data_size = 0+5+12+20+1;
            } else {
                code_of_reregistration = 0;
                tx_data_size = 0+5+12+20+1+1;
            }

            memcpy(&buffer_tx_data[0], (void*)&cur_fd_number, 4);
            memcpy(&buffer_tx_data[4], (void*)&fiscal_mark, 4);
            answer_code = ERR_CODE_00;
            tx_data_size = 8;
            return;
        break;

        case 0x04: // ������ �������� ����������� ������ ��
        /*
        ������� �������� ��������� �������� ����������� ������.
        ������  �����������  ���������  ������  ����  ��������  �  �������
        ������� ��������� ������ ���������.
        ������������ ����� ������ 2 ���������.
        */
            // �������� ��������� ��
            if (FISCAL != State) {
                answer_code = ERR_CODE_02;
                tx_data_size = 0;
                return;
            }
            prev_cmd = buffer_rx[3];
            State = FCLOS;
            
            answer_code = ERR_CODE_00;
            tx_data_size = 0;
            return;
        break;
        case 0x05: //  ������� ���������� ����� ��
        /*
        ������  �������  ���������  �����������  �����  �  ���������  ��  �
        ��������������� �����
        */
            // ������ ��
            if (errno == ERR_CODE_03) {
                answer_code = ERR_CODE_03;
                tx_data_size = 0;
                return;
            }
            // ������ ��
            if (errno == ERR_CODE_04) {
                answer_code = ERR_CODE_04;
                tx_data_size = 0;
                return;
            }
            // �������� ���� �/��� �����
            if (errno == ERR_CODE_07) {
                answer_code = ERR_CODE_07;
                tx_data_size = 0;
                return;
            }
            // �������� ��������� ��
            if (FCLOS != State) {
                answer_code = ERR_CODE_02;
                tx_data_size = 0;
                return;
            }
            prev_cmd = buffer_rx[3];
            
            State = POSTFISCAL;
            life_phase = LIFE_PHASE_POSTFISCAL;
            
            memcpy(date_time_cur, buffer_rx_data, 5);
            memcpy(kkt_number, buffer_rx_data, 20);

            memcpy(&buffer_tx_data[0], (void*)&cur_fd_number, 4);
            memcpy(&buffer_tx_data[4], (void*)&fiscal_mark, 4);
            answer_code = ERR_CODE_00;
            tx_data_size = 8;
            
            return;
        break;

        // ** 6.5 ������� ������������ ���������� ���������� � ��������
        // --------------------------------------------------------------------
        // --------------------------------------------------------------------

        case 0x10: // ������ ���������� ������� �����
        /*
        ������ ������� ��������� ������ ��������� ������� ����� ���. ������
        �������� ������ ������� ����� (���� ���� ��� ��� �������), ���� �� �����
        ������� ����� �����.
        */
            // ������ ��
            if (errno == ERR_CODE_03) {
                answer_code = ERR_CODE_03;
                tx_data_size = 0;
                return;
            }
            // ������ ��
            if (errno == ERR_CODE_04) {
                answer_code = ERR_CODE_04;
                tx_data_size = 0;
                return;
            }
            // ������������ �������� ���������� �������
            if (errno == ERR_CODE_09) {
                answer_code = ERR_CODE_09;
                tx_data_size = 0;
                return;
            }
            // �������� ��������� ��
            if ((FISCAL != State) && 
                (SMENAOPEN != State))
            {
                answer_code = ERR_CODE_02;
                tx_data_size = 0;
                return;
            }
            prev_cmd = buffer_rx[3];
            buffer_tx_data[0] = state_of_work;
            buffer_tx_data[1] = BYTE_1(work_number);
            buffer_tx_data[2] = BYTE_0(work_number);
            buffer_tx_data[3] = BYTE_1(check_number);
            buffer_tx_data[4] = BYTE_0(check_number);

            memcpy(&buffer_tx_data[4], (void*)&fiscal_mark, 4);
            answer_code = ERR_CODE_00;
            tx_data_size = 8;
            return;

        break;
        case 0x11: //  ������ �������� �����
        /*
        ������� �������� ��������� �������� �����.
        */
            // �������� ���� �/��� �����
            if (errno == ERR_CODE_07) {
                answer_code = ERR_CODE_07;
                tx_data_size = 0;
                return;
            }
            // �������� ������ �������� �������� ���������
            if (errno == ERR_CODE_15) {
                answer_code = ERR_CODE_15;
                tx_data_size = 0;
                return;
            }
            // �������� ��������� ��
            if (FISCAL != State) {
                answer_code = ERR_CODE_02;
                tx_data_size = 0;
                return; 

            }
            prev_cmd = buffer_rx[3];
            State = OP1;
            memcpy(date_time_cur, buffer_rx_data, 5);
            answer_code = ERR_CODE_00;
            tx_data_size = 0;
            return;

        break;
        case 0x12: // ������� �����
            /*
            �������, ����������� ��������� �������� �����. ������ �����
            ���������� ������ ������� ���������� ���������� ���������
            ������������ ����� � �������� �����.
            ������� ����������: ������ ���� ��������� ������� ������� ��������
            ������; ������ ���� �������� ������ ���������.
            */
            // ������ ��
            if (errno == ERR_CODE_03) {
                answer_code = ERR_CODE_03;
                tx_data_size = 0;
                return;
            }
            // ������ ��
            if (errno == ERR_CODE_04) {
                answer_code = ERR_CODE_04;
                tx_data_size = 0;
                return;
            }
            // �������� ���� ������������ ��
            if (errno == ERR_CODE_05) {
                answer_code = ERR_CODE_05;
                tx_data_size = 0;
                return;
            }
            // ����� �� ����������
            if (errno == ERR_CODE_06) {
                answer_code = ERR_CODE_06;
                tx_data_size = 0;
                return;
            }
            // �������� ���� �/��� �����
            if (errno == ERR_CODE_07) {
                answer_code = ERR_CODE_07;
                tx_data_size = 0;
                return;
            }
            // �������� ������ �� (������������������ ������������)
            if (errno == ERR_CODE_12) {
                answer_code = ERR_CODE_12;
                tx_data_size = 0;
                return;
            }
            // �������� ������ ��������
            if (errno == ERR_CODE_14) {
                answer_code = ERR_CODE_14;
                tx_data_size = 0;
                return;
            }
            // �������� ������ �������� �������� ���������
            if (errno == ERR_CODE_15) {
                answer_code = ERR_CODE_15;
                tx_data_size = 0;
                return;
            }
            // �������� ��������� ��
            if (OP2 != State) {
                answer_code = ERR_CODE_02;
                tx_data_size = 0;
                return;
            }
            prev_cmd = buffer_rx[3];
            State = SMENAOPEN;
            state_of_work = WORK_OPENED_1;
            memcpy(&buffer_tx_data[0], (void*)&work_number, 2);
            memcpy(&buffer_tx_data[0+2], (void*)&cur_fd_number, 4);
            memcpy(&buffer_tx_data[0+2+4], (void*)&fiscal_mark, 4);
            answer_code = ERR_CODE_00;
            tx_data_size = 0+2+4+4;
            return;

        break;
        case 0x13: // ������ �������� �����
        /*
        ������� �������� ��������� �������� �����. ������� ����������:
        ����� ������ ���� �������; ��� ������ ���� ������; �� ������ ���� �
        ���������� ������.
        */
            // �������� ���� �/��� �����
            if (errno == ERR_CODE_07) {
                answer_code = ERR_CODE_07;
                tx_data_size = 0;
                return;
            }
            // �������� ������ �������� �������� ���������
            if (errno == ERR_CODE_15) {
                answer_code = ERR_CODE_15;
                tx_data_size = 0;
                return;
            }
            // �������� ��������� ��
            if (SMENAOPEN != State) {
                answer_code = ERR_CODE_02;
                tx_data_size = 0;
                return;
            }
            prev_cmd = buffer_rx[3];
            State = CLOS1;
            memcpy(date_time_cur, buffer_rx_data, 5);
            answer_code = ERR_CODE_00;
            tx_data_size = 0;
            return;

        break;
        case 0x14: // ������� �����
        /*
        ������� ��������� ��������� �������� �����. ������� ����������:
        ������ ���� ��������� ������� ������� �������� ������ � ��������� ������
        ���������.
        */
            // ������ ��
            if (errno == ERR_CODE_03) {
                answer_code = ERR_CODE_03;
                tx_data_size = 0;
                return;
            }
            // ������ ��
            if (errno == ERR_CODE_04) {
                answer_code = ERR_CODE_04;
                tx_data_size = 0;
                return;
            }
            // �������� ������ ��������
            if (errno == ERR_CODE_14) {
                answer_code = ERR_CODE_14;
                tx_data_size = 0;
                return;
            }
            // �������� ������ �������� �������� ���������
            if (errno == ERR_CODE_15) {
                answer_code = ERR_CODE_15;
                tx_data_size = 0;
                return;
            }
            // �������� ��������� ��
            if (CLOS2 != State) {
                answer_code = ERR_CODE_02;
                tx_data_size = 0;
                return;
            }
            prev_cmd = buffer_rx[3];
            State = FISCAL;
            state_of_work = WORK_CLOSED_0;
            memcpy(&buffer_tx_data[0], (void*)&work_number, 2);
            memcpy(&buffer_tx_data[0+2], (void*)&cur_fd_number, 4);
            memcpy(&buffer_tx_data[0+2+4], (void*)&fiscal_mark, 4);
            answer_code = ERR_CODE_00;
            tx_data_size = 0+2+4+4;
            return;
        break;
        case 0x15: // ������ ������������ ���� (���)
        /*
        ������� �������� ��������� ������������ ����������� ���������
        ��������� ��� (��� ������ ������� ����������). ������� ����������:
        ����� ������ ���� ������� � �� ��� ����� ������ ���.
        ���� � ����� �� ������ ������������ ����� ��� �� 24 ���� ���� � �����
        �������� ������ �����.
        */
            // �������� ���� �/��� �����
            if (errno == ERR_CODE_07) {
                answer_code = ERR_CODE_07;
                tx_data_size = 0;
                return;
            }
            // �������� ������ �������� �������� ���������
            if (errno == ERR_CODE_15) {
                answer_code = ERR_CODE_15;
                tx_data_size = 0;
                return;
            }
            // ����������������� ����� ����� 24 �����
            if (errno == ERR_CODE_16) {
                answer_code = ERR_CODE_16;
                tx_data_size = 0;
                return;
            }
            // �������� ������� �� ������� ����� 2 ����������
            if (errno == ERR_CODE_17) {
                answer_code = ERR_CODE_17;
                tx_data_size = 0;
                return;
            }
            // �������� ��������� ��
            if (FISCAL != State) {
                answer_code = ERR_CODE_02;
                tx_data_size = 0;
                return;
            }
            prev_cmd = buffer_rx[3];
            State = CHK1;            
            memcpy(date_time_cur, buffer_rx_data, 5);
            answer_code = ERR_CODE_00;
            tx_data_size = 0;
            return;
        break;
        case 0x16: // ������������ ���
        /*
        ������� �������� ����� ����, ��� ��� ������ ���� ���� �������� �
        ������� ������� 15h ��� ������� 17h.
        */
            // ������ ��
            if (errno == ERR_CODE_03) {
                answer_code = ERR_CODE_03;
                tx_data_size = 0;
                return;                
            }
            // ������ ��
            if (errno == ERR_CODE_04) {
                answer_code = ERR_CODE_04;
                tx_data_size = 0;
                return;
            }
            // �������� ���� �/��� �����
            if (errno == ERR_CODE_07) {
                answer_code = ERR_CODE_07;
                tx_data_size = 0;
                return;
            }
            // ������������ �������� ���������� �������
            if (errno == ERR_CODE_09) {
                answer_code = ERR_CODE_09;
                tx_data_size = 0;
                return;
            }
            // �������� ������ �� (������������������ ������������)
            if (errno == ERR_CODE_12) {
                answer_code = ERR_CODE_12;
                tx_data_size = 0;
                return;
            }
            // �������� ������ ��������
            if (errno == ERR_CODE_14) {
                answer_code = ERR_CODE_14;
                tx_data_size = 0;
                return;
            }
            // �������� ��������� ��
            if (CHK1 != State) {
                answer_code = ERR_CODE_02;
                tx_data_size = 0;
                return;
            } 
            prev_cmd = buffer_rx[3];
            State = FISCAL;
            memcpy(&buffer_tx_data[0], (void*)&check_number, 2);
            memcpy(&buffer_tx_data[0+2], (void*)&cur_fd_number, 4);
            memcpy(&buffer_tx_data[0+2+4], (void*)&fiscal_mark, 4);
            answer_code = ERR_CODE_00;
            tx_data_size = 0+2+4+4;
            return;
        break;
        case 0x17: // ������ ������������ ���� ��������� (���)
        /*
        ������� �������� ��������� ������������ ����������� ���������
        ��������� ��� ���������. ������� ����������:
        - ����� ������ ���� ������� � �� ��� ����� ������ ��� (��� ��� ���������).
        - ���� � ����� �� ������ ������������ ����� ��� �� 24 ���� ���� � �����
        �������� ������ �����.
        */
            // �������� ���� �/��� �����
            if (errno == ERR_CODE_07) {
                answer_code = ERR_CODE_07;
                tx_data_size = 0;
                return;
            }
            // �������� ������ �������� �������� ���������
            if (errno == ERR_CODE_15) {
                answer_code = ERR_CODE_15;
                tx_data_size = 0;
                return;
            }
            // ����������������� ����� ����� 24 �����
            if (errno == ERR_CODE_16) {
                answer_code = ERR_CODE_16;
                tx_data_size = 0;
                return;
            }
            // �������� ������� �� ������� ����� 2 ����������
            if (errno == ERR_CODE_17) {
                answer_code = ERR_CODE_17;
                tx_data_size = 0;
                return;
            }
            // �������� ��������� ��
            if (FISCAL != State) {
                answer_code = ERR_CODE_02;
                tx_data_size = 0;
                return;

            } 
            prev_cmd = buffer_rx[3];
            State = CHK1;
            memcpy(date_time_cur, buffer_rx_data, 5);
            answer_code = ERR_CODE_00;
            tx_data_size = 0;
            return;
        break;
        case 0x18: // ������ ������������ ������ � ��������� ��������
        /*
        ������� �������� ��������� ������������ ����������� ���������
        ������ � ��������� ��������. ������� ����������:
        - �� ������ ���� � ��������� ����������� ����� ��� ���������������
        �����
        - ����� ������ ���� �������
        - ������ ����������� ��������� ������ ���� �������� � ������� �������
        ��������� ������ ���������, ������������ ����� ������ 2 ���������
        - ������������ ����� ������ 1 ��������. ������� ����������: �� ������
        ���� � ���������� ������.
        */
            // �������� ���� �/��� �����
            if (errno == ERR_CODE_07) {
                answer_code = ERR_CODE_07;
                tx_data_size = 0;
                return;
            }
            // �������� ��������� ��
            if (SMENAOPEN != State) {
                answer_code = ERR_CODE_02;
                tx_data_size = 0;
                return;
            }
            prev_cmd = buffer_rx[3];
            State = REPORT1;
            memcpy(date_time_cur, buffer_rx_data, 5);
            answer_code = ERR_CODE_00;
            tx_data_size = 0;
            return;
        break;
        case 0x19: // ������������ ����� � ��������� ��������
        /*
        �������, ����������� ��������� ������������ ������ � ���������
        ��������. ������� ����������: ������ ���� ��������� ������� �������
        ������������ ������ � ��������� ��������
        */
            // ������ ��
            if (errno == ERR_CODE_03) {
                answer_code = ERR_CODE_03;
                tx_data_size = 0;
                return;
            }
            // ������ ��
            if (errno == ERR_CODE_04) {
                answer_code = ERR_CODE_04;
                tx_data_size = 0;
                return;
            }
            // �������� ���� ������������ ��
            if (errno == ERR_CODE_05) {
                answer_code = ERR_CODE_05;
                tx_data_size = 0;
                return;
            }
            // ����� �� ����������
            if (errno == ERR_CODE_06) {
                answer_code = ERR_CODE_06;
                tx_data_size = 0;
                return;
            }
            // �������� ���� �/��� �����
            if (errno == ERR_CODE_07) {
                answer_code = ERR_CODE_07;
                tx_data_size = 0;
                return;
            }
            // �������� ������ �� (������������������ ������������)
            if (errno == ERR_CODE_12) {
                answer_code = ERR_CODE_12;
                tx_data_size = 0;
                return;
            }
            // �������� ������ ��������
            if (errno == ERR_CODE_14) {
                answer_code = ERR_CODE_14;
                tx_data_size = 0;
                return;
            }
            // �������� ��������� ��
            if (REPORT2 != State) {
                answer_code = ERR_CODE_02;
                tx_data_size = 0;
                return;
            } 
            prev_cmd = buffer_rx[3];
            State = SMENAOPEN;
            memcpy(&buffer_tx_data[0], (void*)&cur_fd_number, 4);
            memcpy(&buffer_tx_data[0+4], (void*)&fiscal_mark, 4);
            memcpy(&buffer_tx_data[0+4+4], (void*)&num_of_uncommited_docs, 4);
            answer_code = ERR_CODE_00;
            tx_data_size = 0+4+4+4;
            return;
        break;
        case 0x65:
            buffer_tx_data[0] = State;
            answer_code = ERR_CODE_00;
            tx_data_size = 1;
        break;
//
//        // ** 6.6 ������� ��������������� ������ � �������� ���
//        // --------------------------------------------------------------------
//        // --------------------------------------------------------------------
//
//        case 0x20: // �������� ������ ��������������� ������
//        /*
//        ������� ����������� ������� ������ ��������������� ������ �
//        �������� ���. ��������� ������, ���� �� ��������� ��� �������� � ������
//        ���, ���� �� ������� �� ������� ���, ����� ������ �� �������� ������
//        ����������� ����� ������ ��� � ������ ��.
//        */
//            if (errno == ERR_CODE_02) {
//                answer_code = ERR_CODE_02;
//                tx_data_size = 0;
//                return;
//            }
//            if (errno == ERR_CODE_03) {
//                answer_code = ERR_CODE_03;
//                tx_data_size = 0;
//                return;
//            }
//            prev_cmd = buffer_rx[3];
//            buffer_tx_data[0] = exchange_state;
//            buffer_tx_data[1] = read_state;
//            buffer_tx_data[2] = BYTE_1(0);
//            buffer_tx_data[3] = BYTE_0(0);
//
//            memset(&buffer_tx_data[4], 0, 4);
//            memset(&buffer_tx_data[8], 0, 5);
//            return;
//        break;
//        case 0x21: // �������� ������ ������������� ���������� � �������� ���
//        /*
//        ������ ������� ���������� ������� �� �� ������������ ��� �������
//        ������������� ���������� � �������� ���
//        */
//            if (errno == ERR_CODE_02) {
//                answer_code = ERR_CODE_02;
//                tx_data_size = 0;
//                return;
//            }
//            if (errno == ERR_CODE_03) {
//                answer_code = ERR_CODE_03;
//                tx_data_size = 0;
//                return;
//            }
//            prev_cmd = buffer_rx[3];
//            connection_state = buffer_rx_data[0];
//            return;
//        break;
//        case 0x22: // ������ ������ ��������� ��� ������� ���
//        /*
//        ������ ������� �������� ������ ��������� ��� ���. ����� �
//        ���������� �������� � ������� ������ ����� ���������, ������� ������
//        ������ ��������� ��� ������� ���������� ������ ���������
//        */
//            if (errno == ERR_CODE_11) {
//                answer_code = ERR_CODE_11;
//                tx_data_size = 0;
//                return;
//            }
//            if (errno == ERR_CODE_08) {
//                answer_code = ERR_CODE_08;
//                tx_data_size = 0;
//                return;
//            }
//            prev_cmd = buffer_rx[3];
//            buffer_tx_data[0] = 0;
//            buffer_tx_data[1] = 0;
//            return;
//        break;
//        case 0x23: // ��������� ���� ��������� ��� ������� ���
//        /*
//        ������ ������� ������������� ��� ��������� ������� ��������� ���
//        �������� � ���. ����� ��������� ����� � �������� ���������� ���.
//        ���� ����������� ������ ������ ������ ����������� �����, �� �����
//        ��������� ����������� ������ ������.
//        */
//            if (errno == ERR_CODE_11) {
//                answer_code = ERR_CODE_11;
//                tx_data_size = 0;
//                return;
//            }
//            if (errno == ERR_CODE_02) {
//                answer_code = ERR_CODE_02;
//                tx_data_size = 0;
//                return;
//            }
//            if (errno == ERR_CODE_01) {
//                answer_code = ERR_CODE_01;
//                tx_data_size = 0;
//                return;
//            }
//            if (errno == ERR_CODE_03) {
//                answer_code = ERR_CODE_03;
//                tx_data_size = 0;
//                return;
//            }
//            prev_cmd = buffer_rx[3];
//            
//            return;
//        break;
//        case 0x24: // �������� ������ ��������� ��� ������� ���
//        /*
//        ���������� ���� ������� �������� ������� �������� ������
//        ��������� ��� ���.
//        */
//            if (errno == ERR_CODE_02) {
//                answer_code = ERR_CODE_02;
//                tx_data_size = 0;
//                return;
//            }
//            prev_cmd = buffer_rx[3];
//            
//            return;
//        break;
//        case 0x26: // �������� ��������� �� ������� ���
//        /*
//        ������ ������� ������������� ��� �������� � �� ���������
//        (���������) �� ���. ����� ��������� �� ��� ���������� ������������
//        ������ ������ � ����� ������� �� (��. ���������� ��������� ��).
//        */
//            if (errno == ERR_CODE_20) {
//                answer_code = ERR_CODE_20;
//                tx_data_size = 0;
//                return;
//            }
//            if (errno == ERR_CODE_02) {
//                answer_code = ERR_CODE_02;
//                tx_data_size = 0;
//                return;
//            }
//            if (errno == ERR_CODE_03) {
//                answer_code = ERR_CODE_03;
//                tx_data_size = 0;
//                return;
//            }
//            if (errno == ERR_CODE_04) {
//                answer_code = ERR_CODE_04;
//                tx_data_size = 0;
//                return;
//            }
//            if (errno == ERR_CODE_05) {
//                answer_code = ERR_CODE_05;
//                tx_data_size = 0;
//                return;
//            }
//            prev_cmd = buffer_rx[3];
//            
//            return;
//        break;
//
//        // ** 6.7 ������� ��������� ������ �� ������ ��
//        // --------------------------------------------------------------------
//        // --------------------------------------------------------------------
//        case 0x40: // ����� ���������� �������� �� ������
//        /*
//        ������� ��������� ����� � ������ �� ���������� �������� �� ��� ������.
//        */
//            if (errno == ERR_CODE_02) {
//                answer_code = ERR_CODE_02;
//                tx_data_size = 0;
//                return;
//            }
//            if (errno == ERR_CODE_08) {
//                answer_code = ERR_CODE_08;
//                tx_data_size = 0;
//                return;
//            }
//            if (errno == ERR_CODE_03) {
//                answer_code = ERR_CODE_03;
//                tx_data_size = 0;
//                return;
//            }
//            prev_cmd = buffer_rx[3];
//            
//            return;
//        break;
//        case 0x41: // ������ ��������� � ��������� �����������
//                   // ��������� ���������� ������ � ��� �� ������ ���������
//        /*
//        ������� ��������� ����� � ������ �� ���������, �������������� ���������
//          �� � ���.
//        */
//            if (errno == ERR_CODE_02) {
//                answer_code = ERR_CODE_02;
//                tx_data_size = 0;
//                return;
//            }
//            if (errno == ERR_CODE_08) {
//                answer_code = ERR_CODE_08;
//                tx_data_size = 0;
//                return;
//            }
//            if (errno == ERR_CODE_03) {
//                answer_code = ERR_CODE_03;
//                tx_data_size = 0;
//                return;
//            }
//            prev_cmd = buffer_rx[3];
//            
//            return;
//        break;
//        case 0x42: // ������ ���������� ��, �� ������� ��� ���������
//        /*
//        ������� ��������� �������� ���������� ���������� � ������ ��, ��
//        ������� �� �������� ��������� �� ���.
//        */
//            if (errno == ERR_CODE_02) {
//                answer_code = ERR_CODE_02;
//                tx_data_size = 0;
//                return;
//            }
//            prev_cmd = buffer_rx[3];
//            
//            return;
//        break;
//        case 0x43: // ������ ������ ������������ ��
//        /*
//        */
//            if (errno == ERR_CODE_02) {
//                answer_code = ERR_CODE_02;
//                tx_data_size = 0;
//                return;
//            }
//            prev_cmd = buffer_rx[3];
//            
//            return;
//        break;
//        case 0x44: // ������ ��������� ������������ ��
//        /*
//        ��������� �������� �������� TLV ������ �� ����������, �������� ���
//        ������������ � ������� ��������� ������ ������������. ������ ��������
//        ��� ������ ������ ����� ��������� ���������� ������������.
//
//        ���
//
//        ��������� �������� �������� TLV ������ �� ����������, �������� ���
//        ������������ (������������ ������ � ����������� (���������������) ���) �
//        ������� 07h ��������� ������ ����������� ���������. ������ �������� ���
//        ������ ������ ����� ��������� ���������� ������������. ����� ������ �
//        ����������� (���������������), ��� �������� ���������� �������� ������
//        ���������� ������ ����������.
//        */
//            if (errno == ERR_CODE_02) {
//                answer_code = ERR_CODE_02;
//                tx_data_size = 0;
//                return;
//            }
//            if (errno == ERR_CODE_08) {
//                answer_code = ERR_CODE_08;
//                tx_data_size = 0;
//                return;
//            }
//            prev_cmd = buffer_rx[3];
//            
//            return;
//        break;
//        case 0x45: // ������ ����������� ��������� � TLV �������
//        /*
//        ������ ������� ��������� ��������� �� ���������� �����������
//        ��������� � TLV �������, ������� ������ ���������� �� ��� � ������,
//        �������������� ��.
//        */
//            if (errno == ERR_CODE_02) {
//                answer_code = ERR_CODE_02;
//                tx_data_size = 0;
//                return;
//            }
//            if (errno == ERR_CODE_08) {
//                answer_code = ERR_CODE_08;
//                tx_data_size = 0;
//                return;
//            }
//            if (errno == ERR_CODE_03) {
//                answer_code = ERR_CODE_03;
//                tx_data_size = 0;
//                return;
//            }
//            prev_cmd = buffer_rx[3];
//            
//            return;
//        break;
//        case 0x46: // ������ TLV ����������� ���������
//        /*
//        ������ ������� ������������� ��� ��������� ������ �����������
//        ��������� �� ������ ��. �� ���������� ���� ������� ������ ����
//        ��������� ������� 45h.
//        */
//            if (errno == ERR_CODE_02) {
//                answer_code = ERR_CODE_02;
//                tx_data_size = 0;
//                return;
//            }
//            if (errno == ERR_CODE_08) {
//                answer_code = ERR_CODE_08;
//                tx_data_size = 0;
//                return;
//            }
//            if (errno == ERR_CODE_03) {
//                answer_code = ERR_CODE_03;
//                tx_data_size = 0;
//                return;
//            }
//            prev_cmd = buffer_rx[3];
//            
//            return;
//        break;
//        case 0x47: // ������ TLV ���������� ������������
//        /*
//        ������ ������� ������������� ��� ��������� ���� ������, ����������
//        ���, � ������� ������� 07h ����� ����������� ������� 03h.60
//        ����� �������������� ������ ������� ���������� ������� ������� 44h
//        ������� ��������� ������������ �ͻ �� ��������� FFFFh � �������� �������
//        ���������. � ��������� ������ ������� ������ ��� ������ 08h � ���
//        ����������� ������.
//        */
//            if (errno == ERR_CODE_02) {
//                answer_code = ERR_CODE_02;
//                tx_data_size = 0;
//                return;
//            }
//            if (errno == ERR_CODE_08) {
//                answer_code = ERR_CODE_08;
//                tx_data_size = 0;
//                return;
//            }
//            if (errno == ERR_CODE_03) {
//                answer_code = ERR_CODE_03;
//                tx_data_size = 0;
//                return;
//            }
//            prev_cmd = buffer_rx[3];
//            
//            return;
//        break;
//
//        // ** 6.8 ���������� �������
//        // --------------------------------------------------------------------
//        // --------------------------------------------------------------------
//        case 0x60: //  ����� ��������� ��
//        /*
//        ������ ������� ��������� �������� ���� ����� ��� ��������� ��.
//        ������ ������� �������� ������ ��� ���������� ������ �� ��.
//        */
//            if (0x16 == buffer_rx_data[0]) {
//
//            } else {
//
//            }
//            prev_cmd = buffer_rx[3];
//            
//            memset(buffer_tx_data, 'A', 5);
//            tx_data_size = 5;
//            return;
//        break;

    } // end switch
    
    /*
    ����������� �������
    */
    answer_code = ERR_CODE_01;
    tx_data_size = 0;

    return;
}


