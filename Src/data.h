#pragma once

/*
��� 0 � ��������� ��������� ��
��� 1 � ������  ���������� �����
��� 2 � �������������� �����
��� 3 � ���������  �������� ���������� ������ � ���
*/
/*
���������  ���� �����  (��������� ��������  ���� ���������  � ������� 8)
��� 0 � ��������� ��������� ��
��� 1 � ������  ���������� �����
0  0  0  0  ��������� (0)
0  0  0  1  ����������  � ������������ (1)
0  0  1  1  ���������� ����� (3)
0  1  1  1  ��������������  �����, ���� �������� �� � ��� (7)
1  1  1  1  ������ ������ �� ������ �� (15)
*/
extern enum {
  LIFE_PHASE_READY2FISCAL  =0x01,
  LIFE_PHASE_FISCAL  =0x03,
  LIFE_PHASE_POSTFISCAL  =0x07,
  LIFE_PHASE_ARCHIVE =0x0F,
} life_phase;


/*
������� ��������
00h � ��� ��������� ���������
01h � ����� � ����������� ���
02h � ����� �� �������� �����
04h � �������� ���
08h � ����� � �������� �����
10h � �����  �  �������� ����������� ������
11h � ����� ������� ����������
12h - �����  ��  ��������� ����������  �����������  ���  � ����� � ������� ��
13h � �����  ��  ��������� ���������� ����������� ���
14h � �������� ��� ���������
15h � ��� ���������
17h � ����� � ������� ��������� ��������
*/
extern enum {
  CUR_DOC_0  =0x00,
  CUR_DOC_1  =0x01,
  CUR_DOC_2  =0x02,
  CUR_DOC_4  =0x04,
  CUR_DOC_8  =0x08,
  CUR_DOC_10 =0x10,
  CUR_DOC_11 =0x11,
  CUR_DOC_12 =0x12,
  CUR_DOC_13 =0x13,
  CUR_DOC_14 =0x14,
  CUR_DOC_15 =0x15,
  CUR_DOC_17 =0x17,
} cur_doc;

/*
������ ���������
0 � ��� ������ ���������
1 � �������� ������ ���������
*/
extern enum {
  NO_DATA_0    =0x00,
  DATA_RXED_1  =0x01,
} doc_data;

/*
��������� �����
0 � ����� �������
1 � ����� �������
*/
extern enum {
  WORK_CLOSED_0  =0x00,
  WORK_OPENED_1  =0x01,
} state_of_work;


// ����� �����
extern u16 work_number;
// ����� ����
extern u16 check_number;

#define FL_ASSERTS_01 0x01 // ������� ������ ��
#define FL_ASSERTS_02 0x02 // ���������� ������� ��
#define FL_ASSERTS_04 0x04 // ������������ ������ ��, ����� �������� �� 90%
#define FL_ASSERTS_08 0x08 // ��������� ����� �������� ������ ���
#define FL_ASSERTS_80 0x80 // ����������� ������

extern u8 flags_and_asserts;

extern u8 date_expired[3+1];
extern u8 date_time_last_doc[5+1];
extern u8 date_time_cur[5+1];
extern u8 inn_number[12+1];
extern u8 kkt_number[20+1];
extern u32 last_fd_number;

// ����� ��
extern u32 cur_fd_number;

// ���������� ���������������� ����������
extern u32 num_of_uncommited_docs;

// ���������� �������
extern u32 fiscal_mark;
/*
0 � ���������� ������
1 � �������� ������
*/
extern enum {
  DEBUG_0  =0x00,
  DEBUG_1  =0x01,
} debug_mode = DEBUG_0;


extern u8 const MSG_START;

/*
00h �������� ���������� �������
01h ����������� �������, �������� ������ ������� ��� ����������� ���������

02h �������� ��������� ��
03h ������ ��
04h ������ ��
05h �������� ���� ������������ ��
06h ����� �� ����������
07h �������� ���� �/��� �����
08h ��� ����������� ������
09h ������������ �������� ���������� �������
10h ���������� �������� TLV ������
11h ��� ������������� ����������
12h �������� ������ �� (������������������ ������������)

14h �������� ������ ��������
15h �������� ������ �������� �������� ���������
16h ����������������� ����� ����� 24 �����
17h �������� ������� �� ������� ����� 2 ����������
20h ��������� �� ��� �� ����� ���� �������
*/

#define ERR_CODE_00  0x00 // �������� ���������� �������
#define ERR_CODE_01  (0x01 | 0x80)// ����������� �������, �������� ������ ������� ��� ����������� ���������
#define ERR_CODE_02  (0x02 | 0x80) // �������� ��������� ��
#define ERR_CODE_03  (0x03 | 0x80) // ������ ��
#define ERR_CODE_04  (0x04 | 0x80) // ������ ��
#define ERR_CODE_05  (0x05 | 0x80) // �������� ���� ������������ ��
#define ERR_CODE_06  (0x06 | 0x80) // ����� �� ����������
#define ERR_CODE_07  (0x07 | 0x80) // �������� ���� �/��� �����
#define ERR_CODE_08  (0x08 | 0x80) // ��� ����������� ������
#define ERR_CODE_09  (0x09 | 0x80) // ������������ �������� ���������� �������

#define ERR_CODE_10  (0x10 | 0x80) // ���������� �������� TLV ������
#define ERR_CODE_11  (0x11 | 0x80) // ��� ������������� ����������
#define ERR_CODE_12  (0x12 | 0x80) // �������� ������ �� (������������������ ������������)
#define ERR_CODE_14  (0x14 | 0x80) // �������� ������ ��������
#define ERR_CODE_15  (0x15 | 0x80) // �������� ������ �������� �������� ���������
#define ERR_CODE_16  (0x16 | 0x80) // ����������������� ����� ����� 24 �����
#define ERR_CODE_17  (0x17 | 0x80) // �������� ������� �� ������� ����� 2 ����������
#define ERR_CODE_20  (0x20 | 0x80) // ��������� �� ��� �� ����� ���� �������
extern u8 answer_code;
extern u8 errno;

/*
0 0 0 0 0 1 �����
0 0 0 0 1 0 ���������� �����
0 0 0 1 0 0 ���������� ����� ����� ������
0 0 1 0 0 0 ������ ����� �� ��������� �����
0 1 0 0 0 0 ������ �������������������� �����
1 0 0 0 0 0 ��������� ������� ���������������
*/
#define FL_TAX_CODE_01 0x01
#define FL_TAX_CODE_02 0x02
#define FL_TAX_CODE_04 0x04
#define FL_TAX_CODE_08 0x08
#define FL_TAX_CODE_10 0x10
#define FL_TAX_CODE_20 0x20
extern u8 tax_code;

/*
0 0 0 0 0 0 1 ����������
0 0 0 0 0 1 0 ���������� �����
0 0 0 0 1 0 0 �������������� �����
0 0 0 1 0 0 0 ���������� � ����� �����
0 0 1 0 0 0 0 ����� ��� (1) ����� ����� ����� (0)
0 1 0 0 0 0 0 ���������� � ��������
1 0 0 0 0 0 0 ���������� ����������� �������� (�����������)
*/
#define FL_MODE_WORK_01 0x01
#define FL_MODE_WORK_02 0x02
#define FL_MODE_WORK_04 0x04
#define FL_MODE_WORK_08 0x08
#define FL_MODE_WORK_10 0x10
#define FL_MODE_WORK_20 0x20
#define FL_MODE_WORK_40 0x40
extern u8 mode_of_work;

/*
1 ������ ��
2 ����� ���
3 ����� ���������� ������������
4 ����� �������� ���
*/
#define FL_CODE_REREGISTRATION_01 0x01
#define FL_CODE_REREGISTRATION_02 0x02
#define FL_CODE_REREGISTRATION_03 0x03
#define FL_CODE_REREGISTRATION_04 0x04
extern u8 code_of_reregistration;



extern enum {
  OP_TYPE_INCOME = 1,
  OP_TYPE_INCOME_BACK = 2,
  OP_TYPE_OUTCOME = 3,
  OP_TYPE_OUTCOME_BACK = 4,
} op_type;

extern enum {
  EXCHANGE_STATE_0 = 0,
  EXCHANGE_STATE_1 = 1,
  EXCHANGE_STATE_2 = 2,
  EXCHANGE_STATE_3 = 3,
  EXCHANGE_STATE_4 = 4,
  EXCHANGE_STATE_5 = 5,
} exchange_state;

extern enum {
  READ_STATE_0 = 0,
  READ_STATE_1 = 1,
} read_state;

extern enum {
  TRANSFER_STATE_0 = 0,
  TRANSFER_STATE_1 = 1,
} transfer_state;


#define CONNECTION_STATE_0 0
#define CONNECTION_STATE_1 1
extern u8 connection_state;
// ���� ����
extern uint64_t check_result;



#define REPORT_TYPE_00 0
#define REPORT_TYPE_01 1
#define REPORT_TYPE_02 2

extern u8 report_type;



// -----------------------------------
extern enum {
  FISCAL = 0, 
  READY2FISCAL, POSTFISCAL, ARCHIVE, SMENAOPEN,
  OP1, OP2, CLOS1, CLOS2, CHK1, CHK2, REG1, REG2, FCLOS, 
  REPORT1, REPORT2
} State;
