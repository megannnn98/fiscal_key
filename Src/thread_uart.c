


/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "stm32f0xx_hal.h"
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include "pt.h"
#include "common_types.h"
#include "utils.h"

/* Private variables ---------------------------------------------------------*/
extern UART_HandleTypeDef huart1;
extern DMA_HandleTypeDef hdma_usart1_tx;


#define RX_SIZE 1030
extern u8 buffer_rx[RX_SIZE];
extern u8 cnt_rx;
extern u8 uart_rx;

extern u8 buffer_tx[RX_SIZE];
extern u8 tx_data_size;

extern uint32_t time_rx;
extern u8 const MSG_START;
extern u8 answer_code;

void process_rxed_data();

u16 crc()
{
    return 0xFFFF;
}

PT_THREAD (protothread_uart(pt_t *pt))
{
    PT_BEGIN(pt);
    
    extern u32 time_rx;
      
    while(1) {
  
        if (SET == uart_rx) {

            
            while((HAL_GetTick() - time_rx) < 10) {
                PT_YIELD(pt);
            }
            if (cnt_rx > 4) {
                process_rxed_data();

                
                buffer_tx[0] = MSG_START;
                buffer_tx[1] = BYTE_1(tx_data_size+1);
                buffer_tx[2] = BYTE_0(tx_data_size+1);
                buffer_tx[3] = answer_code;
                
                buffer_tx[6 + tx_data_size - 2] = BYTE_1(crc());
                buffer_tx[6 + tx_data_size - 1] = BYTE_0(crc());
                
                HAL_UART_Transmit_DMA(&huart1, buffer_tx, tx_data_size + 6);
                PT_WAIT_UNTIL(pt, USART_ISR_TC == (huart1.Instance->ISR & USART_ISR_TC));
            }
            
            memset(buffer_tx, 0, sizeof(buffer_tx));
            uart_rx = RESET;
            cnt_rx = 0;
            memset(buffer_rx, 0, sizeof(buffer_rx));
        }
        PT_YIELD(pt);
    } // END WHILE(1)
    PT_END(pt);
} // timer thread




