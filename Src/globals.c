

#include "common_types.h"
/*
���������  ���� �����  (��������� ��������  ���� ���������  � ������� 8)
��� 0 � ��������� ��������� ��
��� 1 � ������  ���������� �����
0  0  0  0  ��������� (0)
0  0  0  1  ����������  � ������������ (1)
0  0  1  1  ���������� ����� (3)
0  1  1  1  ��������������  �����, ���� �������� �� � ��� (7)
1  1  1  1  ������ ������ �� ������ �� (15)
*/
enum {
  LIFE_PHASE_READY2FISCAL  =0x01,
  LIFE_PHASE_FISCAL  =0x03,
  LIFE_PHASE_POSTFISCAL  =0x07,
  LIFE_PHASE_ARCHIVE =0x0F,
} life_phase = LIFE_PHASE_READY2FISCAL;

/*
������� ��������
00h � ��� ��������� ���������
01h � ����� � ����������� ���
02h � ����� �� �������� �����
04h � �������� ���
08h � ����� � �������� �����
10h � �����  �  �������� ����������� ������
11h � ����� ������� ����������
12h - �����  ��  ��������� ����������  �����������  ���  � ����� � ������� ��
13h � �����  ��  ��������� ���������� ����������� ���
14h � �������� ��� ���������
15h � ��� ���������
17h � ����� � ������� ��������� ��������
*/
enum {
  CUR_DOC_0  =0x00,
  CUR_DOC_1  =0x01,
  CUR_DOC_2  =0x02,
  CUR_DOC_4  =0x04,
  CUR_DOC_8  =0x08,
  CUR_DOC_10 =0x10,
  CUR_DOC_11 =0x11,
  CUR_DOC_12 =0x12,
  CUR_DOC_13 =0x13,
  CUR_DOC_14 =0x14,
  CUR_DOC_15 =0x15,
  CUR_DOC_17 =0x17,
} cur_doc = CUR_DOC_0;


// -----------------------------------
enum {
  FISCAL = 0, 
  READY2FISCAL, POSTFISCAL, ARCHIVE, SMENAOPEN,
  OP1, OP2, CLOS1, CLOS2, CHK1, CHK2, REG1, REG2, FCLOS, 
  REPORT1, REPORT2
} State = READY2FISCAL;



/*
������ ���������
0 � ��� ������ ���������
1 � �������� ������ ���������
*/
enum {
  NO_DATA_0    =0x00,
  DATA_RXED_1  =0x01,
} doc_data = NO_DATA_0;



/*
��������� �����
0 � ����� �������
1 � ����� �������
*/
enum {
  WORK_CLOSED_0  =0x00,
  WORK_OPENED_1  =0x01,
} state_of_work = WORK_CLOSED_0;

enum {
  EXCHANGE_STATE_0 = 0,
  EXCHANGE_STATE_1 = 1,
  EXCHANGE_STATE_2 = 2,
  EXCHANGE_STATE_3 = 3,
  EXCHANGE_STATE_4 = 4,
  EXCHANGE_STATE_5 = 5,
} exchange_state;

enum {
  READ_STATE_0 = 0,
  READ_STATE_1 = 1,
} read_state;


// ����� �����
u16 work_number = 0;
// ����� ����
u16 check_number = 0;


u8 flags_and_asserts = 0;

u8 date_expired[3+1] = "000";
u8 date_time_last_doc[5+1] = "00000";
u8 date_time_cur[5+1] = "00000";
u8 inn_number[12+1] = "";
u8 kkt_number[20+1] = "";
u32 last_fd_number = 0;



// ����� ��
u32 cur_fd_number = 0;

// ���������� ���������������� ����������
u32 num_of_uncommited_docs = 0;

// ���������� �������
u32 fiscal_mark = 0;

u8 const MSG_START = 0x04;

u8 answer_code = 0;
u8 errno;

u8 tax_code = 0;
u8 mode_of_work = 0;
u8 code_of_reregistration = 0;
u8 connection_state = 0;
u8 report_type = 0;

// ���� ����
uint64_t check_result;
