


#pragma once

/* Includes ------------------------------------------------------------*/

#include "stm32f0xx.h"
#include <stdbool.h>
#include "pt.h"
#include "common_types.h"

/* Global define ------------------------------------------------------------*/

#define DEBUG 1
#define SLOW_START_WITH_SMS 1
#define IWDG_ON 1
//#define USE_DISPATCHER






#define UART_TIMEOUT        (1000U)
#define MAX_SIZE_HDLC       128


/* Global define ------------------------------------------------------------*/
enum {
    SEC  = (1000UL),
    MIN  = (60*SEC),
    HOUR = (60*MIN),
};



/* Global variables ---------------------------------------------------------*/


/* Global define ------------------------------------------------------------*/



enum
{
  GENERAL_TIMER = 0,
  
  MAX_TIMERS
};

enum
{
  GENERAL_MSG = 0, 
  RX_03_MSG = 0, 
  RX_04_MSG = 0, 
  
  MAX_MSG
};

enum {
    PR_MAX = 0,
    PR_HIGH,
    PR_MEDIUM,
    PR_LOW,
    PR_LOWEST,
};

#define PR_MIN_RATE (16)


/* Global macro -------------------------------------------------------------*/
#ifndef PRINT

#define PRINT(str, size) do { if (DEBUG ) {send_string(str);} } while(0)
#endif


#define PT_DELAY_MS(timer, ms)	do { tim.reset(timer); \
								 PT_WAIT_WHILE(pt, tim.get(timer) < ms); } while(0)

#define PT_STATE_INIT() 	 static pt_t val_pt = {.lc = 0}; \
							 static pt_t *pt = &val_pt;

#define BYTE_0(n)                 ((uint8_t)((n) & (uint8_t)0xFF))        /*!< Returns the low byte of the 32-bit value */
#define BYTE_1(n)                 ((uint8_t)(BYTE_0((n) >> (uint8_t)8)))  /*!< Returns the second byte of the 32-bit value */
#define BYTE_2(n)                 ((uint8_t)(BYTE_0((n) >> (uint8_t)16))) /*!< Returns the third byte of the 32-bit value */
#define BYTE_3(n)                 ((uint8_t)(BYTE_0((n) >> (uint8_t)24))) /*!< Returns the high byte of the 32-bit value */

#define PT_YIELD_SPAWN(pt)                     \
   do {                                          \
     PT_YIELD_FLAG = 0;                          \
     LC_SET((pt)->lc);                           \
     if(PT_YIELD_FLAG == 0) {                    \
       return PT_WAITING;                        \
     }                                           \
   } while(0)
     
#define PT_EXIT_OK(pt)                             \
   do {                                          \
     PT_INIT(pt);                                \
     return PT_EXITED_OK;                   \
   } while(0)
     
#define PT_EXIT_BAD(pt)                             \
   do {                                          \
     PT_INIT(pt);                                \
     return PT_EXITED_BAD;                   \
   } while(0)
     
#define PT_EXIT_TIMEOUT(pt)                             \
   do {                                          \
     PT_INIT(pt);                                \
     return PT_EXITED_TIMEOUT;                   \
   } while(0)

     
#define PT_EXIT_BREAK(pt)                             \
   do {                                          \
     PT_INIT(pt);                                \
     return PT_EXITED_BREAK;                   \
   } while(0)
     
 #define PT_YIELD_WAIT(pt)                       \
   do {                                          \
     PT_YIELD_FLAG = 0;                          \
     LC_SET((pt)->lc);                           \
     if(PT_YIELD_FLAG == 0) {                    \
       return PT_WAITING;                        \
     }                                           \
   } while(0)
     
#define PT_YIELD_MAIN(pt) PT_YIELD(pt);
     
#define ATOMIC_START() do { __disable_irq()
#define ATOMIC_STOP()  __enable_irq(); } while(0)
     
#define SWAP_8(a1, a2, tmp) do {  tmp = a1; \
                                  a1 = a2;  \
                                  a2 = tmp; } while(0)
    
        
                                  
#define IS_SUBSTR(str, substr) ( NULL != (strstr((char*)str, substr)))


// ������������� ������������ � ������������
//#define PT_INIT(pt, priority)   LC_INIT((pt)->lc ; (pt)->pri = priority)
//PT_PRIORITY_INIT
#define PT_RATE_INIT() char pt_pri_count = 0;
// maitain proority frame count
//PT_PRIORITY_LOOP ������������ ������� ����������
#define PT_RATE_LOOP() do { pt_pri_count = (pt_pri_count+1) & 0xf; } while(0);
// ��������� ����� � �����������
//PT_PRIORITY_SCHEDULE
// 5 �������
// �������� 0 ����� ������� -- ����������� ������ ����
// ��������� 1 -- ������  2 ����� 
// ��������� 2 -- ������  4 ������
// ��������� 3 -- ������  8 ������
// ��������� 4 -- ������ 16 ������
#define PT_PR_EXECUTE(rate, code) \
   do { if((PR_MAX     == rate) | \
           (PR_HIGH    == rate && (0 == (pt_pri_count & 0x01))) | \
           (PR_MEDIUM  == rate && (0 == (pt_pri_count & 0x03))) | \
           (PR_LOW     == rate && (0 == (pt_pri_count & 0x07))) | \
           (PR_LOWEST  == rate && (0 == (pt_pri_count & 0x0F)))) \
           {code;}; } while(0);


#define EXECUTE_AND_IF_ERR_RETURN(code) \
   do { if (ERROR_WHILE_TRUE == code) return ERROR_WHILE_TRUE; } while(0);

             
/* Global functions  ---------------------------------------------------------*/

     

void send_string(u8 s[]);
void send_c(u8 c);


/* Global drivers  ---------------------------------------------------------*/


/**
  * @{ @brief  ��������� ������� MSG.
*/
struct Msg_driver
{
	void (*init)();
    void (*process)(); 
    u8   (*set)(const u8 imsg, void* iparamPtr, u16 iparamSize);
    u8   (*repeat)(const u8 imsg);
    
	bool (*getVal)(const u8 imsg)	 ;
	void* (*getPtr)(const u8 imsg) ;
	u16   (*getSize)(const u8 imsg);
};
/**
  * @}
  */

extern const struct Msg_driver msg;







